package ru.tulinov.enums;

import lombok.Getter;

@Getter
public enum Statuses {
    WAIT("WAIT"),
    COMPLETED("COMPLETED");

    private final String value;

    Statuses(String value) {
        this.value = value;
    }

}
