package ru.tulinov.controllers;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import ru.tulinov.dtos.ClaimDTO;
import ru.tulinov.dtos.ClaimResponseDTO;
import ru.tulinov.services.ClaimService;

@RestController
@RequestMapping("/api/v1/claims")
@Slf4j
public class ClaimController {

    private final ClaimService claimService;

    @Autowired
    public ClaimController(ClaimService claimService) {
        this.claimService = claimService;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.OK)
    public String registerClaim(@RequestBody ClaimDTO claimDTO) {
        return claimService.registerClaim(claimDTO);
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public ClaimResponseDTO getClaimStatus(@RequestParam String number) {
        return claimService.getClaimStatusByNumber(number);
    }

}
