package ru.tulinov.services;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.server.ResponseStatusException;
import ru.tulinov.dtos.ClaimDTO;
import ru.tulinov.dtos.ClaimResponseDTO;
import ru.tulinov.enums.Statuses;
import ru.tulinov.models.Claim;
import ru.tulinov.repositories.ClaimRepository;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
@Slf4j
public class ClaimService {

    private static final int FIXED_RATE = 60000;
    private final ClaimRepository claimRepository;

    @Autowired
    public ClaimService(ClaimRepository claimRepository) {
        this.claimRepository = claimRepository;
    }

    public String registerClaim(ClaimDTO claimDTO) {
        log.info("Регистрация в iis");
        Claim recivedClaim = Claim.builder()
                .number(claimDTO.getNumber())
                .typeId(claimDTO.getClaimType())
                .departmentId(claimDTO.getDepartmentCode())
                .statusId(claimDTO.getClaimStatus())
                .passportNumber(claimDTO.getPassportNumber())
                .authorFullName(claimDTO.getAuthorFullName())
                .build();

        recivedClaim.setRegistrationDate(LocalDateTime.now());
        recivedClaim.setStatusId(Statuses.WAIT.getValue());
        claimRepository.save(recivedClaim);
        log.info("Заявление успешно зарегистрировано");
        return "Заявление успешно зарегистрировано";
    }

    @Scheduled(fixedRate = FIXED_RATE)
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void claimsProcessing() {
        log.info("Обработка заявлений");
        List<Claim> claimsNeedToUpdateStatus = claimRepository.findByStatusIdLastTwoMinutes(Statuses.WAIT.getValue());
        for (Claim claim : claimsNeedToUpdateStatus) {
            claim.setStatusId(Statuses.COMPLETED.getValue());
            log.info("Заявление {} успешно обработано", claim.getNumber());
            claimRepository.save(claim);
        }
    }

    public ClaimResponseDTO getClaimStatusByNumber(String number) {
        log.trace("Проверка существования заявлеения");
        Optional<Claim> claim = claimRepository.findClaimByNumber(number);
        if (claim.isEmpty()) {
            log.warn("Заявление не найдено");
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Заявление не найден");
        }
        log.info("Получен статус {} для заявления с номером {}", claim.get().getStatusId(), claim.get().getNumber());
        return new ClaimResponseDTO(claim.get().getStatusId());
    }

    @ExceptionHandler(ResponseStatusException.class)
    public ResponseEntity<String> handleResponseStatusException(ResponseStatusException e) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
    }
}