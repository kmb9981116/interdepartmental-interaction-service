package ru.tulinov.models;

import javax.persistence.Id;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Column;
import java.time.LocalDateTime;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.Builder;

@Entity
@Table(name = "t_claims")
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Claim {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "number", nullable = false, unique = true)
    private String number;
    @Column(name = "type_id", nullable = false, length = 50)
    private String typeId;
    @Column(name = "department_id", nullable = false, length = 50)
    private String departmentId;
    @Column(name = "status_id", nullable = false, length = 50)
    private String statusId;
    @Column(name = "passport_number", nullable = false, length = 50)
    private String passportNumber;
    @Column(name = "author_full_name", nullable = false)
    private String authorFullName;
    @Column(name = "registration_date", nullable = false)
    private LocalDateTime registrationDate;
}