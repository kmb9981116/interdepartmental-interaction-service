package ru.tulinov.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.Builder;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "t_claims_types")
@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ClaimType {
    @Id
    @Column(length = 50)
    private String code;
    @Column(length = 100, nullable = false)
    private String name;
}
