package ru.tulinov.dtos;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.Builder;
import org.springframework.stereotype.Component;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@Component
public class ClaimDTO {
    private String number;
    private String claimType;
    private String departmentCode;
    private String claimStatus;
    private String passportNumber;
    private String authorFullName;
}