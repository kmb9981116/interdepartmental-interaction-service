package ru.tulinov.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.tulinov.models.Claim;

import java.util.List;
import java.util.Optional;

@Repository
public interface ClaimRepository extends JpaRepository<Claim, Long> {

    Optional<Claim> findClaimByNumber(String number);

    @Query(value = "SELECT * FROM t_claims t " +
            "WHERE t.status_id = ?1 " +
            "AND t.registration_date <= (NOW() - INTERVAL '2' MINUTE)", nativeQuery = true)
    List<Claim> findByStatusIdLastTwoMinutes(String statusId);

}