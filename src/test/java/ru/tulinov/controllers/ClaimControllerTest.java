package ru.tulinov.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.server.ResponseStatusException;
import ru.tulinov.dtos.ClaimDTO;
import ru.tulinov.dtos.ClaimResponseDTO;
import ru.tulinov.enums.Statuses;
import ru.tulinov.services.ClaimService;

import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(MockitoExtension.class)
class ClaimControllerTest {

    @Mock
    private ClaimService claimService;

    @InjectMocks
    private ClaimController claimController;

    private MockMvc mockMvc;
    private ObjectMapper objectMapper;

    @BeforeEach
    void setUp() {
        mockMvc = MockMvcBuilders.standaloneSetup(claimController).build();
        objectMapper = new ObjectMapper();
    }

    @Test
    void registerClaimSuccessful() throws Exception {
        ClaimDTO claimDTO = ClaimDTO.builder()
                .number("123")
                .passportNumber("123123")
                .authorFullName("authorFullName")
                .claimType("DOC-PFR")
                .departmentCode("DEP-PFR")
                .claimStatus("123")
                .build();
        String jsonContent = objectMapper.writeValueAsString(claimDTO);
        String response = "Заявление успешно зарегистрировано";

        Mockito.when(claimService.registerClaim(any(ClaimDTO.class))).thenReturn(response);

        mockMvc.perform(post("/api/v1/claims")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonContent))
                .andExpect(status().isOk());
    }

    @Test
    void getClaimStatusSuccessful() throws Exception {
        String number = "123";
        ClaimResponseDTO claimResponseDTO = new ClaimResponseDTO(Statuses.COMPLETED.getValue());
        Mockito.when(claimService.getClaimStatusByNumber(number)).thenReturn(claimResponseDTO);

        mockMvc.perform(get("/api/v1/claims")
                        .param("number", number)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.claimStatus").value(Statuses.COMPLETED.getValue()));

    }

    @Test
    void getClaimStatusNumberNotFound() throws Exception {
        String number = "123";

        Mockito.when(claimService.getClaimStatusByNumber(number))
                .thenThrow(new ResponseStatusException(HttpStatus.NOT_FOUND));

        mockMvc.perform(get("/api/v1/claims")
                        .param("number", number)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }
}